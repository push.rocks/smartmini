/// <reference types="uglify-js" />
import 'typings-global';
import * as uglifyJs from 'uglify-js';
export declare class Smartmini {
    /**
     * the constructor for Smartmini
     */
    constructor();
    /**
     * minifies JavaScript from a file
     */
    minifyFromJsFile(filePathArg: string): uglifyJs.MinifyOutput;
    /**
     * minifies JavaScript from a string
     */
    minifyFromJsString(stringToMinifyArg: string): uglifyJs.MinifyOutput;
}
