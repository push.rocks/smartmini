import 'typings-global'
import * as uglifyJs from 'uglify-js'

export class Smartmini {

    /**
     * the constructor for Smartmini
     */
    constructor() {}

    /**
     * minifies JavaScript from a file
     */
    minifyFromJsFile(filePathArg: string): uglifyJs.MinifyOutput {
        return uglifyJs.minify(filePathArg)
    }


    /**
     * minifies JavaScript from a string
     */
    minifyFromJsString(stringToMinifyArg: string): uglifyJs.MinifyOutput {
        return uglifyJs.minify(stringToMinifyArg, {fromString: true})
    }
}